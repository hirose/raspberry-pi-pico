# Raspberry Pi Pico

Used to use ampy, but rshell is more useful.
$python3 -m pip install rshell

In the rshell prompt, we can use the command below to install a custom program.
```
cp [local_file] /pyboard/[name of a python file]
```
Copying a file with a name "/pyboard/main.py" on the Pico by rshell,
the script to be executed automatically after RP Pico boot.
With a name "/pyboard/boot.py", it's executed right after the boot (even before the main.py execution).

In the REPL mode of rshell, hit Ctrl+X to exit.

------------------------------------
For the serial output monitoring, we can use minicom.
brew install minicom

For the first time, you can setup minicom by ...
$minicom -s
Then change the hardware flow control to be NO.
Otherwise, any keyboard interuption won't be accepted...

------------------------------------
How to HARD RESET

import machine
machine.reset()
